﻿set from = "twodopeshaggy@gmail.com"
set realname = "John Jenkins"
set imap_user = "twodopeshaggy@gmail.com"
set imap_pass = "noway"
set folder = "imaps://imap.gmail.com:993"
set spoolfile = "+INBOX"
set postponed ="+[Gmail]/Drafts"
#set trash = "imaps://imap.gmail.com/[Gmail]/Trash"
set header_cache =~/.mutt/cache/headers
set message_cachedir =~/.mutt/cache/bodies
set certificate_file =~/.mutt/certificates
set smtp_url = "smtp://twodopeshaggy@smtp.gmail.com:587/"
set smtp_pass = "nopasswordforyou"
#set editor="nano -t +8" 
set editor="vim"
set timeout = 120
set mail_check = 60
unset imap_passive
bind index G imap-fetch-mail
macro pager \cb <pipe-entry>'w3m'<enter> 'Follow links in w3m'
set date_format="%y-%m-%d %T"
set index_format="%2C | %Z %-30.30F (%-4.4c) %s [%d]"

bind index,pager S flag-message # star a message

#speed up folder switch time
set sleep_time = 0

#set sidebar_visible=no
set sort = threads
auto_view text/html
## Abook
set query_command= "abook --mutt-query '%s'"
macro index,pager  a "<pipe-message>abook --add-email-quiet<return>" "Add this sender to Abook"
bind editor        <Tab> complete-query
##goobook
#set query_command="goobook query '%s'"
#macro index,pager a "<pipe-message>goobook add<return>" "add sender to google contacts"
#bind editor <Tab> complete-query

bind editor <space> noop
macro index gi "<change-folder>=INBOX<enter>" "Go to inbox"
macro index ga "<change-folder>=[Gmail]/All Mail<enter>" "Go to all mail"
macro index gs "<change-folder>=[Gmail]/Sent Mail<enter>" "Go to Sent Mail"
macro index gd "<change-folder>=[Gmail]/Drafts<enter>" "Go to drafts"
macro index gt "<change-folder>=SMS<enter>" "Go to SMS"
macro index gm "<change-folder>=MailingLists<enter>" "Go to MailingLists"

set move = no  #Stop asking to "move read messages to mbox"!
set imap_keepalive = 900

folder-hook +INBOX 'macro index,pager y "<save-message>=[Gmail]/All Mail<enter><enter>" "Archive conversation"'
macro index,pager e  "<save-message>=[Gmail]/All Mail<enter><enter>" "Archive conversation"




# -[ DEFAULT COLOR DEFINITIONS ]-----------------------------------------------

color hdrdefault brightmagenta         default
color quoted     cyan           default
color quoted1    blue        default
color quoted2    yellow         default
color quoted3    red            default
color quoted4    cyan           default
color signature  red            default
color indicator  blue      default
color attachment red          default
color error      yellow         default
color message    white          default
color search     white          default
color status     blue         default
color tree       blue         default
color normal     white          default
color tilde      black          default
color bold       white          default
color markers    red            default


# -[ COLOUR DEFINITIONS WHEN ON A MONO SCREEN ]--------------------------------

#mono bold        bold
#mono underline   underline
#mono indicator   reverse

# -[ COLOURS FOR ITEMS IN THE READER ]-----------------------------------------

color header     red            default         "^X-Junked-Because:"
#mono  header     bold                           "^(From|Subject|X-Junked-Because):"
color header     white          default         '^(status|lines|date|received|sender|references):'
color header     brightblue        default         '^from:'
color header     brightgreen          default         '^(to|cc|bcc):'
color header     magenta        default         '^(subject):.*$'

# -[ COLOURS FOR ITEMS IN THE INDEX ]------------------------------------------

# Regular new messages
#color index cyan          default   "~N !~T !~F !~p !~P"

# Regular tagged messages
#color index red           default    "~T !~F !~p !~P"

# Regular flagged messages
#color index magenta       default     "~F !~p !~P"

# Messages to me
#color index white         default "~p !~N !~T !~F !~P"
#color index brightcyan    default "~p ~N !~T !~F !~P"
#color index white         default "~p ~T !~F !~P"
#color index white         default "~p ~F !~P"

# Messages from me
#color index white         default "~P !~N !~T !~F"
#color index white         default "~P ~N !~T !~F"
#color index white         default "~P ~T !~F"
#color index white         default "~P ~F"

# Messages which mention my name in the body
#color index yellow        default "~b \"phil(_g|\!| gregory| gold)|pgregory\" !~N !~T !~F !~p !~P"
#color index yellow        default "~b \"phil(_g|\!| gregory| gold)|pgregory\" ~N !~T !~F !~p !~P"
#color index yellow        default "~b \"phil(_g|\!| gregory| gold)|pgregory\" ~T !~F !~p !~P"
#color index yellow        default "~b \"phil(_g|\!| gregory| gold)|pgregory\" ~F !~p !~P"

# Messages which are in reference to my mails
#color index magenta       default "~x \"(mithrandir|aragorn)\\.aperiodic\\.net|thorin\\.hillmgt\\.com\" !~N !~T !~F !~p !~P"
#color index magenta       default "~x \"(mithrandir|aragorn)\\.aperiodic\\.net|thorin\\.hillmgt\\.com\" ~N !~T !~F !~p !~P"
#color index magenta       default "~x \"(mithrandir|aragorn)\\.aperiodic\\.net|thorin\\.hillmgt\\.com\" ~T !~F !~p !~P"
#color index magenta       default "~x \"(mithrandir|aragorn)\\.aperiodic\\.net|thorin\\.hillmgt\\.com\" ~F !~p !~P"

# Messages to root, etc.
#color index cyan          default "~C \"(root|postmaster|abuse|mailer-daemon)@\" !~N !~P !~p"
#color index cyan          default "~C \"(root|postmaster|abuse|mailer-daemon)@\" ~N !~P !~p"

# Big messages
#color index cyan          default "!~N ~z 102400-"
#color index cyan          default "~T !~F !~p !~P ~z 102400-"
#color index cyan          default "~N ~z 102400-"

# Deleted messages
#color index red           default "!~N ~D"
#color index red           default "~N ~D"


# -[ HIGHLIGHTS INSIDE THE BODY OF A MESSAGE. ]--------------------------------

#color body      brightmagenta   default         "(http|https|ftp|news|telnet|finger)://[^ \">\t\n]*"
#color body      brightgreen     default         "mailto:[-a-z_0-9.]+@[-a-z_0-9.]+"
#color body      brightyellow    default         "news:[^ \">\t\n]*"
#mono  body      bold                            "(http|https|ftp|news|telnet|finger)://[^ \">\t\n]*"
#mono  body      bold                            "mailto:[-a-z_0-9.]+@[-a-z_0-9.]+"
#mono  body      bold                            "news:[^ \">\t\n]*"

# -[ EMAIL ADDRESSES ]---------------------------------------------------------

color body      brightblue      default         "[-a-z_0-9.%$]+@[-a-z_0-9.]+\\.[-a-z][-a-z]+"
#mono  body      bold                            "[-a-z_0-9.%$]+@[-a-z_0-9.]+\\.[-a-z][-a-z]+"

# -[ VARIOUS SMILIES AND THE LIKE ]--------------------------------------------

#color body      brightgreen     default         "<[Gg]>"                                                # <g>
#color body      brightgreen     default         "<[Bb][Gg]>"                                            # <bg>
#color body      brightgreen     default         " [;:]-*[)>(<|]"                                        # :-) etc...
#color body      brightgreen     default         "(^|[[:space:]])\\*[^[:space:]]+\\*([[:space:]]|$)"     # *Bold* text.
#color body      brightgreen     default         "(^|[[:space:]])_[^[:space:]]+_([[:space:]]|$)"         # _Underlined_ text.
