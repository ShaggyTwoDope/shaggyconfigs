SAVEHIST=10000
HISTFILE=~/.zsh_history

# if exists, add ~/bin to PATH
if [ -d ~/bin ] ; then
   PATH=~/bin:$PATH
fi

if [ -d ~/.gem/ruby/1.9.1/bin ] ; then
   PATH=~/.gem/ruby/1.9.1/bin:$PATH
fi
## clipbored options for dmenurl/dmenuclip
#export CLIPBORED_DMENU_FONT=terminus
export CLIPBORED_DMENU_NORMAL_FG=#4FFF64

ZSH_HIGHLIGHT_HIGHLIGHTERS=(main brackets pattern cursor)

bindkey -e

# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '~/.zshrc'
zstyle ':completion:*' menu select
zstyle ':completion:*:pacman:*' force-list always
zstyle ':completion:*:*:pacman:*' menu yes select
zstyle ':completion:*' completer _complete _match _approximate
zstyle ':completion:*:match:*' original only
zstyle ':completion:*:approximate:*' max-errors 1 numeric
zstyle ':completion:*:functions' ignored-patterns '_*'
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*'   force-list always
# match uppercase from lowercase
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
# 1. All /etc/hosts hostnames are in autocomplete
if [ -f $HOME/.ssh/known_hosts ] ; then
    hosts=(${${${(f)"$(<$HOME/.ssh/known_hosts)"}%%\ *}%%,*})
    zstyle ':completion:*:hosts' hosts $hosts
fi
# ignore completion functions (until the _ignored completer)
zstyle ':completion:*:functions' ignored-patterns '_*'
zstyle ':completion:*:*:*:users' ignored-patterns \
        adm apache bin daemon games gdm halt ident junkbust lp mail mailnull \
        named news nfsnobody nobody nscd ntp operator pcap postgres radvd \
        rpc rpcuser rpm shutdown squid sshd sync uucp vcsa xfs avahi-autoipd\
        avahi backup messagebus beagleindex debian-tor dhcp dnsmasq fetchmail\
        firebird gnats haldaemon hplip irc klog list man cupsys postfix\
        proxy syslog www-data mldonkey sys snort

setopt completealiases
setopt autocd

autoload -Uz compinit
compinit

autoload -U promptinit
promptinit

#autoload -U colors && colors
# create a zkbd compatible hash;
# to add other keys to this hash, see: man 5 terminfo
typeset -A key

key[Home]=${terminfo[khome]}

key[End]=${terminfo[kend]}
key[Insert]=${terminfo[kich1]}
key[Delete]=${terminfo[kdch1]}
key[Up]=${terminfo[kcuu1]}
key[Down]=${terminfo[kcud1]}
key[Left]=${terminfo[kcub1]}
key[Right]=${terminfo[kcuf1]}
key[PageUp]=${terminfo[kpp]}
key[PageDown]=${terminfo[knp]}

# setup key accordingly
[[ -n "${key[Home]}"    ]]  && bindkey  "${key[Home]}"    beginning-of-line
[[ -n "${key[End]}"     ]]  && bindkey  "${key[End]}"     end-of-line
[[ -n "${key[Insert]}"  ]]  && bindkey  "${key[Insert]}"  overwrite-mode
[[ -n "${key[Delete]}"  ]]  && bindkey  "${key[Delete]}"  delete-char
[[ -n "${key[Up]}"      ]]  && bindkey  "${key[Up]}"      up-line-or-history
[[ -n "${key[Down]}"    ]]  && bindkey  "${key[Down]}"    down-line-or-history
[[ -n "${key[Left]}"    ]]  && bindkey  "${key[Left]}"    backward-char
[[ -n "${key[Right]}"   ]]  && bindkey  "${key[Right]}"   forward-char

# Finally, make sure the terminal is in application mode, when zle is
# active. Only then are the values from $terminfo valid.

function zle-line-init () {
    echoti smkx
}
function zle-line-finish () {
    echoti rmkx
}

zle -N zle-line-init
zle -N zle-line-finish

watch=all
LOGCHECK=60
# End of lines added by compinstall
# This will set the default prompt to the walters theme
LS_COLORS='rs=0:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:tw=30;42:ow=34;42:st=37;44:ex=01;32:';
export LS_COLORS

prompt_char(){
    [[ -n $vcs_info_msg_0_ ]] && echo '%F{green}↘ ' && return
    echo '%F{green}↘ '
}

vi-git-status () {
  # Untracked files.
  if [[ -n $(git ls-files --other --exclude-standard 2> /dev/null) ]]; then
    hook_com[unstaged]='%F{r}?%f'
  fi
}

prompt_gtmanfred_precmd(){
    vcs_info
}

prompt_gtmanfred_help(){
  cat <<EOH
gtmanfred's prompt
EOH
}

prompt_gtmanfred_setup() {
    setopt prompt_subst
    autoload -U colors && colors
    autoload -Uz add-zsh-hook vcs_info

    prompt_opts=(cr percent subst)
    add-zsh-hook precmd prompt_gtmanfred_precmd

    zstyle ':vcs_info:*' enable bzr git hg svn
    zstyle ':vcs_info:*' check-for-changes true
    zstyle ':vcs_info:*' stagedstr '%F{g}●%f'
    zstyle ':vcs_info:*' unstagedstr '%F{y}!%f'
    zstyle ':vcs_info:*' formats 'on %F{m}%b%c%u%F{n}'
    zstyle ':vcs_info:*' actionformats "%b%c%u|%F{c}%a%f"
    zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat '%b|%F{c}%r%f'
    zstyle ':vcs_info:git*+set-message:*' hooks git-status

    PROMPT='%B%F{blue}%~%f%b ${vcs_info_msg_0_}%{$reset_color%}$prompt_newline$(prompt_char)%f'

    #RPROMPT="%(?,%F{blue}シャギー二つドープ,%F{yellow}%? %F{red}マザーファッカー%f"
    RPROMPT="%(?,%F{blue}♥,%F{yellow}%? %F{red}＃%f"
    PS4='+%N:%i:%x:%I>'
}

prompt_gtmanfred_preview(){
    prompt_preview_theme gtmanfred "$@"
}
prompt_gtmanfred_setup "$@"

# Window title
case $TERM in
    termite|*xterm*|rxvt|rxvt-unicode|rxvt-256color|rxvt-unicode-256color|(dt|k|E)term)
		precmd () { print -Pn "\e]0;[%n@%M][%~]%#\a" } 
		preexec () { print -Pn "\e]0;[%n@%M][%~]%# ($1)\a" }
	;;
    screen)
    	precmd () { 
			print -Pn "\e]83;title \"$1\"\a" 
			print -Pn "\e]0;$TERM - (%L) [%n@%M]%# [%~]\a" 
		}
		preexec () { 
			print -Pn "\e]83;title \"$1\"\a" 
			print -Pn "\e]0;$TERM - (%L) [%n@%M]%# [%~] ($1)\a" 
		}
	;; 
esac

# hub tab-completion script for zsh.
# This script complements the completion script that ships with git.

# Autoload _git completion functions
if declare -f _git > /dev/null; then
  _git
fi

if declare -f _git_commands > /dev/null; then
  _hub_commands=(
    'alias:show shell instructions for wrapping git'
    'pull-request:open a pull request on GitHub'
    'fork:fork origin repo on GitHub'
    'create:create new repo on GitHub for the current project'
    'browse:browse the project on GitHub'
    'compare:open GitHub compare view'
  )
  # Extend the '_git_commands' function with hub commands
  eval "$(declare -f _git_commands | sed -e 's/base_commands=(/base_commands=(${_hub_commands} /')"
fi

set -o notify 


# usage: remind <time> <text>
# e.g.: remind 10m "omg, the pizza"
function remind() {
    sleep $1 && notify-send "$2" &
}


#source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
#source /usr/share/doc/pkgfile/command-not-found.zsh
source ~/.aliasrc



PATH="${PATH}:$(find ${HOME}/bin -type d | tr '\n' ':' | sed 's/:$//')"

export GNUSTEP_USER_ROOT="${HOME}/GNUstep"